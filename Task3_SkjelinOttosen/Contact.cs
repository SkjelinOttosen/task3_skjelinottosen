﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task3_SkjelinOttosen
{
    class Contact
    {
        public string Firstname { get; set; } 
        public string LastName { get; set; }

        public Contact(string firstname, string lastname) 
        {
            Firstname = firstname;
            LastName = lastname;
        }

        public override string ToString()
        {
            return Firstname +" "+ LastName;
        }
    }
}
