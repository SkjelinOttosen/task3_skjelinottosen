﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;

namespace Task3_SkjelinOttosen
{
    class Program
    {
        static void Main(string[] args)
        {      
            // Creates Contacs
            Contact john = new Contact("John", "Mayer");
            Contact sharon = new Contact("Sharon", "Stone");
            Contact steve = new Contact("Steve", "Jobs");
            Contact bill = new Contact("Bill", "Gates");
            Contact jim = new Contact("Jim", "Carrey");

            YellowPage yellowPage2020 = new YellowPage();
            yellowPage2020.AddContacts(john);
            yellowPage2020.AddContacts(sharon);
            yellowPage2020.AddContacts(steve);
            yellowPage2020.AddContacts(bill);
            yellowPage2020.AddContacts(jim);
         
            while (true) 
            {
                Console.WriteLine("Search for a contact: ");
                string searchInput = Console.ReadLine().ToLower();
                yellowPage2020.Search(searchInput);
            }        
        }
    }
}
